# Layout Builder Paragraphs

Core Layout Builder does not support paragraphs. Thankfully
[Paragraph blocks](https://www.drupal.org/project/paragraph_blocks) module fills
this gap. However, the authoring experience can be clunky when paragraph content
needs to be added in the node edit form and its layout position is configured in
the layout builder form.

This module aims to provide smoother authoring experience for using paragraphs
with layout builder.


## How is this module different than Layout Paragraphs?

[Layout Paragraphs](https://www.drupal.org/project/layout_paragraphs) adds the
ability to use and arrange paragraphs in content layout using its own code and
it does not leverage on core Layout Builder. The advantage is that it can
achieve nested layout while core Layout Builder still does not have nested.

This module tries to leverage on core Layout Builder as it should provide better
support and compatibility with other Layout Builder modules. We have seen how
ubercart vs commerce modules, or field_collection vs paragraphs module fared in
the past. It is usually better to provide extended functionality to core
features instead of building the system from scratch.


## What are the limitations of this module?

-   It does not support nested layout as long as core Layout Builder does not.
    Workaround: consider using mini_layouts or add more layout options a la
    radix theme.
-   It completely replaces (hides) the core layout builder inline blocks with
    paragraphs in the layout builder interface.
-   In 1.x the paragraphs are added to a fixed hardcoded field_contents field in
    the parent entity.
-   In 1.x the paragraph population is still rather clunky: paragraph edit form
    opens in a separate page as we try to avoid the nested modal issue when for
    some CKEditor buttons or media library modal.


## Roadmap

For 2.x:

1.  Make sure it's compatible with D10.
2.  Create field widget (tentatively) with its constraint (only one field
    widget type per bundle) that will recognise the field to add the paragraphs
    to.
3.  Leverage on other modules (like layout_builder_iframe_modal or
    layout_builder_modal) to provide the modal interface when editing the
    paragraph content.
4.  Work on the plugin filter hooks functions or leverage on other modules
    (perhaps block_list_override) to streamline the block filter at layout
    builder.
5.  Add some automated tests!


## Credits

This module was first inspired by the presentation of
[Nathan Dentzau (nathandentzau)](https://www.drupal.org/u/nathandentzau) at
[NEDCamp 2019](https://nedcamp.org/sessions/2019/advanced-techniques-layout-builder).


## Requirements

This module requires the following modules:

-   Core Layout Builder
-   [Paragraphs](https://www.drupal.org/project/paragraphs)
-   [Paragraph blocks](https://www.drupal.org/project/paragraph_blocks)


## Recommended modules

Some other modules that can improve the authoring experience of layout builder,
which is compatible with this module:

-   [Paragraphs Edit](https://www.drupal.org/project/paragraphs_edit)
-   [Layout Builder Base](https://www.drupal.org/project/layout_builder_base)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1.  Enable the module.
2.  Follow the instruction of Paragraph blocks to:
    -   set the maximum number of paragraphs to minimum 30
    -   check the "Suppress label field on layout manager block placement."
    -   use with the layout builder setting per entity
3.  Create some paragraph bundles if not available yet.
4.  Create content type with `field_contents` as the machine name for a
    paragraph field (entity reference revisions field), enable paragraph blocks
    for this field.
