<?php

declare(strict_types = 1);

namespace Drupal\layout_builder_paragraphs\Form;

use Drupal\layout_builder\Form\UpdateBlockForm;
use Drupal\layout_builder_paragraphs\Controller\ModalLayoutRebuildTrait;

/**
 * Provides an update block form using modals.
 */
class ModalUpdateBlockForm extends UpdateBlockForm {

  use ModalLayoutRebuildTrait;

}
