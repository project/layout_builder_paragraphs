<?php

declare(strict_types = 1);

namespace Drupal\layout_builder_paragraphs\Form;

use Drupal\layout_builder\Form\MoveBlockForm;
use Drupal\layout_builder_paragraphs\Controller\ModalLayoutRebuildTrait;

/**
 * Provides a move block form using modals.
 */
class ModalMoveBlockForm  extends MoveBlockForm {

  use ModalLayoutRebuildTrait;

}
