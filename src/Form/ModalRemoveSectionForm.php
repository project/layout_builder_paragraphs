<?php

declare(strict_types = 1);

namespace Drupal\layout_builder_paragraphs\Form;

use Drupal\layout_builder\Form\RemoveSectionForm;
use Drupal\layout_builder_paragraphs\Controller\ModalLayoutRebuildTrait;

/**
 * Provides a remove section form using modals.
 */
class ModalRemoveSectionForm extends RemoveSectionForm {

  use ModalLayoutRebuildTrait;

}
