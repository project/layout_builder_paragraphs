<?php

declare(strict_types = 1);

namespace Drupal\layout_builder_paragraphs\Form;

use Drupal\layout_builder\Form\AddBlockForm;
use Drupal\layout_builder_paragraphs\Controller\ModalLayoutRebuildTrait;

/**
 * Provides an add block form using modals.
 */
class ModalAddBlockForm extends AddBlockForm {

  use ModalLayoutRebuildTrait;

}
