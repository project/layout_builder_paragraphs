<?php

/**
 * This controller is not necessary for PO as we don't need modal for add section
 * and add block links.
 */


declare(strict_types = 1);

namespace Drupal\layout_builder_paragraphs\Element;

use Drupal\layout_builder\Element\LayoutBuilder;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Provides a modal layout builder element.
 *
 * @RenderElement("modal_layout_builder")
 */
class ModalLayoutBuilder extends LayoutBuilder {

  /**
   * {@inheritdoc}
   */
  protected function buildAdministrativeSection(SectionStorageInterface $section_storage, $delta) {
    $build = parent::buildAdministrativeSection($section_storage, $delta);

    // Use the modal for removing a section.
    $build['remove']['#attributes']['data-dialog-type'] = 'modal';
    unset($build['remove']['#attributes']['data-dialog-renderer']);

    return $build;
  }

}
