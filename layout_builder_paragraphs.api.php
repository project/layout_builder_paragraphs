<?php

/**
 * @file
 * Hooks provided by layout_builder_paragraphs.
 */

/**
 * Alter the list of the allowed plugins shown in the layout builder sidebar.
 * @param array &$allowed_plugin_ids
 *   Array of regex patterns to match the allowed plugins.
 * @return void
 */
function hook_layout_builder_paragraphs_allowed_plugins_alter(array &$allowed_plugin_ids) {
  // Add allowed blocks when user has certain permission.
  if \Drupal::currentUser()->hasPermission('some privilege permission') {
    $allowed_plugin_ids = array_merge($allowed_plugin_ids, [
      '^menu',                               // menus
      '^mini_layout',                        // mini layout
      '^webform_block',                      // webform
    ]);
  }
}

/**
 * Alter the list of the DISallowed plugins shown in the layout builder sidebar.
 * @param array &$disallowed_plugin_ids
 *   Array of regex patterns to match the disallowed plugins.
 * @return void
 */
function hook_layout_builder_paragraphs_allowed_plugins_alter(array &$disallowed_plugin_ids) {
  // Add some disallowed blocks.
  $disallowed_plugin_ids = array_merge($disallowed_plugin_ids, [
    '^views_block',                        // views block
  ]);
  // Remove the default disallowed blocks.
  if (isset($disallowed_plugin_ids['views_block:content_recent-block_1']))
    unset($disallowed_plugin_ids['views_block:content_recent-block_1']);
}
