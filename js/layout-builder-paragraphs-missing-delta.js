/**
 * @file
 * BTHR chat widget js.
 */

(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.layout_builder_paragraphs_missing_delta = {
    attach: function (context, settings) {
      // Add warning class to the closest tr parent of the paragraph delta
      // widget.
      $('.missing-delta-in-layout').closest('tr').addClass('color-warning');
    }
  };

}(jQuery, Drupal, this, this.document));
